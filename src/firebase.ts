import { initializeApp } from 'firebase/app'
import { getFirestore, collection } from 'firebase/firestore'

export const firebaseApp = initializeApp({
  apiKey: 'AIzaSyBfcmktat4XtAP7X-5tpLeCn8_AIh-G0a4',
  authDomain: 'shapes-destiny.firebaseapp.com',
  projectId: 'shapes-destiny',
  storageBucket: 'shapes-destiny.appspot.com',
  messagingSenderId: '484099990328',
  appId: '1:484099990328:web:1f0494a0c791d2a72717cd'
})

const db = getFirestore(firebaseApp)

export const sessionsRef = collection(db, 'sessions')
